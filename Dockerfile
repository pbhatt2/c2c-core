FROM python:3.7.9-alpine3.12

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN apk update && apk add --no-cache build-base=0.5-r2

RUN python3 -m pip install -r /app/requirements.txt \
    && rm -rf /root/.cache/pip

COPY . /app/

EXPOSE 8080

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8080", "--workers", "4", "--timeout", "300"]