.PHONY: all create venv lint test run shell clean build install docker
SHELL=/bin/bash


VENV_NAME?=venv
VENV_BIN=$(shell pwd)/${VENV_NAME}/bin
VENV_ACTIVATE=. ${VENV_BIN}/activate
UVICORN_PORT=8080
UVICORN_WORKERS=4
UVICORN_TIME_OUT=300

PYTHON=${VENV_BIN}/python3


all:
	@echo "make create"
	@echo "    Create python virtual environment"
	@echo "make install"
	@echo "    Activate Env and install depedencies"
	@echo "make lint"
	@echo "    Run lint on project."
	@echo "make test"
	@echo "    Run tests on project."
	@echo "make run"
	@echo "    Run server."
	@echo "make clean"
	@echo "    Remove python artifacts and virtualenv."


create:
	which python3 || apt install -y python3 python3-pip
	which virtualenv || python3 -m pip install virtualenv==20.0.21

install: create
	test -d $(VENV_NAME) || virtualenv -p python3 $(VENV_NAME)
	${PYTHON} -m pip install -U pip setuptools pylint mypy
	${PYTHON} -m pip install -r requirements.txt
	touch $(VENV_NAME)/bin/activate

lint:
	which pylint || ${PYTHON} -m pip install pylint
	which mypy || ${PYTHON} -m pip install mypy
	${PYTHON} -m pylint app
	${PYTHON} -m mypy --ignore-missing-imports app


test: install
	${PYTHON} -m pytest -vv tests


run: install
	${PYTHON} -m uvicorn --host 0.0.0.0 --port ${UVICORN_PORT} app.main:app --workers ${UVICORN_WORKERS} --timeout ${UVICORN_TIME_OUT}


clean:
	find . -name '*.pyc' -exec rm -f {} +
	rm -rf $(VENV_NAME) *.eggs *.egg-info dist build docs/_build .cache