from fastapi import APIRouter, Body, Depends, Path, Query
from pip._internal.cli.status_codes import SUCCESS

from app.models.schemas import Request, Response , BidCreateRequest , GetAllBidsResponse
from app.crud.postgres import get_repository
from app.crud.repositories.bid_repository import BidRepository

router = APIRouter()


@router.post("/", response_model=Response)
async def create_bid(request: BidCreateRequest = Body(...),bid_repo: BidRepository = Depends(get_repository(BidRepository))):
    ans = await bid_repo.create_bid(request)
    return Response(status="OK")


@router.get("/getTopBids/{sku_id}", response_model=GetAllBidsResponse)
async def get_top_bids(sku_id: str = Path(...), limit: int = 3, bid_repo: BidRepository = Depends(get_repository(BidRepository))):
    ans = await bid_repo.get_top_bids(sku_id, limit)
    print(ans)
    return GetAllBidsResponse(bids=ans)
