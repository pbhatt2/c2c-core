from fastapi import APIRouter, Body, Depends, Path
from pip._internal.cli.status_codes import SUCCESS

from app.crud.repositories.search import SearchService
from app.models.schemas import Request, Response, ProductCreateRequest, GetAllProductsResponse, SkuCreateRequest, \
    ProductCreateResponse, SkuResponse
from app.crud.postgres import get_repository
from app.crud.repositories.product_repository import ProductRepository

router = APIRouter()


@router.post("/product/create", response_model=ProductCreateResponse)
async def create_product(request: ProductCreateRequest = Body(...),
                         product_repo: ProductRepository = Depends(get_repository(ProductRepository))):
    ans = await product_repo.create_product(request)
    return ProductCreateResponse(status="OK",product_id=ans)


@router.get("/product", response_model=GetAllProductsResponse)
async def get_products(product_repo: ProductRepository = Depends(get_repository(ProductRepository))):
    ans = await product_repo.get_products()
    return GetAllProductsResponse(products=ans)


@router.post("/sku/create", response_model=SkuResponse)
async def create_sku(request: SkuCreateRequest = Body(...),
                     product_repo: ProductRepository = Depends(get_repository(ProductRepository)),
                     search_service=Depends(SearchService)):
    sku_id = await product_repo.create_sku(search_service, request)
    return SkuResponse(status="OK", sku_id=sku_id)


@router.get("/sku/{sku_id}")
async def get_sku(sku_id: str = Path(...),
                     product_repo: ProductRepository = Depends(get_repository(ProductRepository))):
    sku = await product_repo.get_sku(sku_id)
    return sku
