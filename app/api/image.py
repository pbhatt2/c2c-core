from fastapi import APIRouter, Body, Depends, HTTPException
from pip._internal.cli.status_codes import SUCCESS
import uuid
from app.crud.postgres import get_repository
from app.crud.repositories.image_repository import ImageRepository
from app.models.image_schema import Request, Response
from app.services.image_service import isImageValid
import cloudinary
import cloudinary.uploader

router = APIRouter()


@router.post("/upload", response_model=Response)
async def upload_images(request: Request = Body(...),
                        image_repo: ImageRepository = Depends(get_repository(ImageRepository))):
    image_string = request.imageData
    cloudinary.config( 
        cloud_name = "dw0eflyms", 
        api_key = "465132821426489", 
        api_secret = "A_PsjTx2V2f7vA14xED0RCfUjCk" 
    )
    imgRes = isImageValid(image_string)
    print("Image Res",imgRes)
    try:
        cloudinary_response = cloudinary.uploader.upload(image_string, upload_preset='sku_media')
        cloudinary_url = cloudinary_response.get('url')
        if not cloudinary_url:
            raise HTTPException(status_code=400, detail="unable to upload image")
            image_id = str(uuid.uuid4())
        #image_res = await image_repo.save_image(request.skuId, image_id, cloudinary_url)
        #if image_res == -1:
        #    raise HTTPException(status_code=400, detail="unable to save to db")s
         #else:
        return Response(status="OK", image_url=cloudinary_url ,confidence=imgRes["confidence"],predicted_class=imgRes["predictedclass"], predicted_category=imgRes["predictedcategory"])
    except Exception as e:
        print(e)
        raise HTTPException(status_code=400,detail="unable to upload the image")