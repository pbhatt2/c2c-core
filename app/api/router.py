from fastapi import APIRouter

from app.api import seller, catalog, bid, search, image, user

api_router = APIRouter()

api_router.include_router(user.router, tags=["C2C"], prefix="/user")
api_router.include_router(seller.router, tags=["C2C"], prefix="/seller")
api_router.include_router(catalog.router, tags=["C2C"], prefix="/catalog")
api_router.include_router(bid.router, tags=["C2C"], prefix="/bid")
api_router.include_router(search.router, tags=["C2C"], prefix="/search")
api_router.include_router(image.router, tags=["C2C"], prefix="/image")
