from fastapi import APIRouter, Body, Depends

from app.crud.repositories.search import SearchService
from app.models.schemas import SearchRequest, SearchResponse

router = APIRouter()


@router.post("/", response_model=SearchResponse)
async def create_bid(request: SearchRequest = Body(...),
                     search_service=Depends(SearchService)):
    documents = await search_service.search(request)
    return SearchResponse(status="OK", data=documents)
