from fastapi import APIRouter, Body, Depends, HTTPException
from pip._internal.cli.status_codes import SUCCESS
import uuid

from app.crud.postgres import get_repository
from app.crud.repositories.seller_repository import SellerRepository
from app.models.seller_login_schema import SellerLoginRequest, SellerLoginResponse
from app.models.seller_schema import SellerRegisterRequest, SellerRegisterResponse

router = APIRouter()

@router.post("/login", response_model=SellerLoginResponse)
async def login_seller(request: SellerLoginRequest = Body(...),
                          seller_repo: SellerRepository = Depends(get_repository(SellerRepository))):
    record = await seller_repo.get_seller(request.email, request.password)
    if len(record) <= 0:
        raise HTTPException(status_code=401, detail="seller does not exists")
    seller_dict = record[0]
    id = seller_dict.get('sellerId')
    name = seller_dict.get('name')
    return SellerLoginResponse(status="OK", seller_id=str(id), name=str(name))


@router.post("/register", response_model=SellerRegisterResponse)
async def register_seller(request: SellerRegisterRequest = Body(...),
                          seller_repo: SellerRepository = Depends(get_repository(SellerRepository))):
    random_id = str(uuid.uuid4())
    id = await seller_repo.save_seller(random_id, request.name, request.email, request.password, request.mobile, request.address, request.shipmentMethod)
    if id is -1:
        raise HTTPException(status_code=403, detail="seller already exists")
    return SellerRegisterResponse(status="OK", seller_id=str(id))
