from fastapi import APIRouter, Body, Depends, HTTPException

from app.crud.postgres import get_repository
from app.crud.repositories.user_repository import UserRepository
from app.models.user_login_schema import UserLoginRequest, UserLoginResponse


router = APIRouter()

@router.post("/login", response_model=UserLoginResponse)
async def login_user(request: UserLoginRequest = Body(...),
                          user_repo: UserRepository = Depends(get_repository(UserRepository))):
    record = await user_repo.get_user(request.email, request.password)
    if len(record) <= 0:
        raise HTTPException(status_code=401, detail="user does not exists")
    user_dict = record[0]
    id = user_dict.get('userId')
    name = user_dict.get('name')
    return UserLoginResponse(status="OK", user_id=str(id), name=str(name))

