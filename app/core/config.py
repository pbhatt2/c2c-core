# pylint: disable=raise-missing-from
import os
from loguru import logger
from app.utils.util import Singleton, read_json


class Config(metaclass=Singleton):

    __BASE_CONFIG_FILE = "resources/config/base-config.json"
    __BASE_DIR = '/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[:-1]) + "/"

    def __init__(self):
        self._conf = self._load_config(self.__BASE_CONFIG_FILE)

    def _load_config(self, fname):
        try:
            return read_json(self.__BASE_DIR + fname)
        except Exception as e:
            print(e)
            raise Exception(f'Failed to read Config File, Shutting down. {self.__BASE_DIR + fname}')

    def get_key(self, key):
        if self._conf is None:
            return None

        keys = key.split(".")
        value = self._conf
        try:
            for k in keys:
                value = value[k]
            return value
        except Exception as e:
            logger.error(e)
            raise KeyError(f'Failed to find key [{key}] in core file')
