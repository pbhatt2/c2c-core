import os
from urllib.parse import quote_plus

from elasticsearch import Elasticsearch
from loguru import logger

import asyncpg
from fastapi import FastAPI

from app.core.config import Config

global_conf = Config()


class ESConnection:
    client:Elasticsearch = None


es = ESConnection()


async def connect_to_es():
    es.client = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    if es.client.ping():
        print('Yay Connected')
    else:
        print('Awww it could not connect!')


async def connect_to_db(app: FastAPI) -> None:
    host = os.environ.get('DB_HOST', 'localhost')
    user = os.environ.get('DB_USER', 'postgres')
    db_name = os.environ.get('DB_NAME', 'postgres')
    psql_password = quote_plus(os.environ.get('DB_PASSWORD', 'password'))
    db_url = f'postgresql://{user}:{psql_password}@{host}/{db_name}'
    db_config = global_conf.get_key('psql_db')

    app.state.pool = await asyncpg.create_pool(
        str(db_url),
        min_size=db_config.get('minPoolSize', 5),
        max_size=db_config.get('maxPoolSize', 10),
    )

    logger.info("Connection established")


async def close_db_connection(app: FastAPI) -> None:
    logger.info("Closing connection to database")

    await app.state.pool.close()

    logger.info("Connection closed")
