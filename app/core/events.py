# pylint: disable=unused-argument
from typing import Callable

from fastapi import FastAPI
from loguru import logger

from app.core.connections import connect_to_db, close_db_connection, connect_to_es


def create_start_app_handler(app: FastAPI) -> Callable:  # type: ignore
    async def start_app() -> None:
        await connect_to_db(app)
        await connect_to_es()
    return start_app


def create_stop_app_handler(app: FastAPI) -> Callable:  # type: ignore
    @logger.catch
    async def stop_app() -> None:
        await close_db_connection(app)

    return stop_app
