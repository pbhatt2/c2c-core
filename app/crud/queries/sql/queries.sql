-- name: get_seller_by_email
SELECT
    seller_id,
    name
FROM
    seller
WHERE
    email = :email
AND 
    password = :password;

-- name: get_user_by_email
SELECT
    user_id,
    name
FROM
    users
WHERE
    email = :email
AND 
    password = :password;


-- name: create_product
Insert into product ( product_id  , name  , description , brand_name , categories ) 
values ( :productid, :name, :description, :brandname, :categories);

-- name: get_products
select * from product;
    
-- name: validate_seller^
SELECT
    seller_id,
    name
FROM
    seller
WHERE
    seller_id = :id
OR 
    email = :email
OR
    mobile = :mobile;

-- name: save_seller<!
INSERT INTO
    seller(seller_id, name, mobile, email, password, address, shipment_method)
VALUES (:id, :name, :mobile, :email, :password, :address, :shipment_method)
RETURNING seller_id;

-- name: create_bid
INSERT INTO
    bid (bid_id, cust_id, sku_id, bid_price, currency , isaccepted)
VALUES (:bid_id, :customer_id, :sku_id, :bid_price, :currency, :accepted)

-- name: get_bid
SELECT * FROM bid WHERE cust_id = :customer_id AND sku_id = :sku_id;

-- name: update_bid
UPDATE bid SET bid_price = :bid_price
WHERE  cust_id = :customer_id AND sku_id = :sku_id

-- name: get_top_bids
SELECT * FROM bid  WHERE sku_id=:sku_id ORDER BY bid_price DESC LIMIT :limit;

--name: get_images_by_skuId
SELECT * FROM images WHERE sku_id = :skuId;

--name: save_image
INSERT INTO
    images(sku_id, image_id, url)
VALUES (:skuId, :imageId, :url);

-- name: create_sku
INSERT INTO sku (sku_id ,seller_id ,product_id , bid_enabled ,bid_start_date, bid_end_date, data)
values ( :sku_id, :seller_id, :product_id, :bid_enabled, :bid_start_date, :bid_end_date, :data);

-- name: get_sku
SELECT * FROM sku WHERE sku_id =:sku_id;