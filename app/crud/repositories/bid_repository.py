# pylint: disable=useless-super-delegation,redefined-builtin,duplicate-code
from json import loads
from typing import List

from asyncpg import Connection
from asyncpg.protocol.protocol import Record

from app.core.errors import EntityDoesNotExist
from app.crud.queries.queries import queries
from app.crud.repositories.BaseRepository import BaseRepository
import uuid


class BidRepository(BaseRepository):

    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def create_bid(self, body):
        
        bid_id = str(uuid.uuid4())
        bids = await queries.get_bid(self.connection, customer_id=body.cust_id, sku_id=body.sku_id)
        print(bids)
        if len(bids) > 0:
            records = await queries.update_bid(self.connection, bid_price=body.bid_price,
                                               customer_id=body.cust_id, sku_id=body.sku_id)
        else:
            records = await queries.create_bid(self.connection, bid_id=bid_id, customer_id=body.cust_id,
                                               sku_id=body.sku_id,
                                               bid_price=body.bid_price, currency=body.currency,
                                               accepted=body.isaccepted)
        if records is None:
            raise "Not inserted"
        return True

    async def get_top_bids(self, sku_id, limit):
        records = await queries.get_top_bids(self.connection, sku_id, limit)
        if records is None:
            return []
        return records