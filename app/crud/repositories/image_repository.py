# pylint: disable=useless-super-delegation,redefined-builtin,duplicate-code
from json import loads
from typing import List

from asyncpg import Connection
from asyncpg.protocol.protocol import Record

from app.core.errors import EntityDoesNotExist
from app.crud.queries.queries import queries
from app.crud.repositories.BaseRepository import BaseRepository


class ImageRepository(BaseRepository):

    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_images_by_skuId(self, skuId) -> List:
        records = await queries.get_images(self.connection, skuId)
        if records is None:
            records = []

        return [self.map_row(r) for r in records]


    async def save_image(self, skuId, imageId, url) -> int:
        image_id = await queries.save_image(self.connection, skuId, imageId, url)
        if image_id is None:
            image_id = -1
        return image_id

    @staticmethod
    def map_row(row: Record):
        return {
            'imageId': row.get('image_id'),
            'url': row.get('url')
        }
