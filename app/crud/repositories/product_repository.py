# pylint: disable=useless-super-delegation,redefined-builtin,duplicate-code
import json
from json import loads
from random import randint
from typing import List

from asyncpg import Connection
from asyncpg.protocol.protocol import Record

from app.core.errors import EntityDoesNotExist
from app.crud.queries.queries import queries
from app.crud.repositories.BaseRepository import BaseRepository
import uuid

from app.utils.util import flatten


class ProductRepository(BaseRepository):

    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def create_product(self, request):
        if request.product_id is None:
            request.product_id = str(uuid.uuid4())
        records = await queries.create_product(self.connection,
                                               productid=request.product_id,
                                               name=request.name, description=request.description,
                                               brandname=request.brand_name, categories=request.categories)
        if records is None:
            raise Exception("Not inserted")
        return request.product_id

    async def get_products(self):
        records = await queries.get_products(self.connection)
        if records is None:
            return []
        return records

    async def create_sku(self, search_service, request):
        if request.sku_id is None:
            request.sku_id = "sku" + str(randint(999999, 999999999))
        data = request.json()
        records = await queries.create_sku(self.connection, sku_id=request.sku_id,
                                           seller_id=request.seller_id, product_id=request.product_id,
                                           bid_enabled=request.bid.enabled, bid_start_date=request.bid.startDate,
                                           bid_end_date=request.bid.endDate, data=data)
        if records is None:
            raise Exception("Not inserted")
        data = flatten(request.json())
        res = await search_service.index(request.sku_id, data)
        return request.sku_id

    async def get_sku(self, sku_id):
        records = await queries.get_sku(self.connection, sku_id)
        if records is None:
            raise EntityDoesNotExist(f'No matching seller for: {sku_id}')
        row = records[0]
        return json.loads(row['data'])
