import json

from app.core.connections import es


class SearchService:
    def __init__(self):
        self.client = es.client

    async def search(self, request):
        response = {}
        data = []
        query = self.get_search_query(request)
        res = self.client.search(index='c2c-products', body=json.dumps(query))
        for hit in res['hits']['hits']:
            data.append(hit['_source'])
        response['docs'] = data
        response['aggs'] = res['aggregations']
        return response

    async def index(self, id, document):
        res = self.client.index(index='c2c-products', id=id, body=document)
        return res['result']

    @staticmethod
    def get_search_query(request):
        query = {"query": {"bool": {"must":[]}},
                 "aggs": {"brandName": {"terms": {"field": "brand_name"}},
                          "categories": {"terms": {"field": "categories"}},
                          "used": {"terms": {"field": "is_used"}},
                          "condition": {"terms": {"field": "condition"}}},
                 "sort": [{"seller_rating": {"order": "desc"}}]}
        if request.query is not None:
            multi_match = {"multi_match": {"query": request.query, "fields": ["brand_name.search", "name.search",
                                                                              "description.search"]}}
            query['query']['bool']['must'].append(multi_match)
        if request.category is not None:
            f = {"terms": {"categories": request.category}}
            query['query']['bool']['must'].append(f)
        if request.filters is not None:
            for f in request.filters:
                f_query = {"terms": {f.key: f.values}}
                query['query']['bool']['must'].append(f_query)
        return query