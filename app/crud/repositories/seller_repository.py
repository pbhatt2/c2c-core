# pylint: disable=useless-super-delegation,redefined-builtin,duplicate-code
from json import loads
from typing import List

from asyncpg import Connection
from asyncpg.protocol.protocol import Record

from app.core.errors import EntityDoesNotExist
from app.crud.queries.queries import queries
from app.crud.repositories.BaseRepository import BaseRepository


class SellerRepository(BaseRepository):

    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_seller(self, email, password) -> List:
        records = await queries.get_seller_by_email(self.connection, email, password)
        if records is None:
            return []
        return [self.map_row(r) for r in records]


    async def save_seller(self, id, name, email, password, mobile, address, shipment_method) -> int:
        seller_record = await queries.validate_seller(self.connection, id, email, mobile)
        if seller_record is not None:
            return -1
        seller_id = await queries.save_seller(self.connection, id, name, mobile, email, password, address, shipment_method)
        if seller_id is None:
            raise Exception('Value not inserted in db')
        return seller_id

    @staticmethod
    def map_row(row: Record):
        return {
            'sellerId': row.get('seller_id'),
            'name': row.get('name')
        }
