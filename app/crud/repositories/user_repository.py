# pylint: disable=useless-super-delegation,redefined-builtin,duplicate-code
from json import loads
from typing import List

from asyncpg import Connection
from asyncpg.protocol.protocol import Record

from app.core.errors import EntityDoesNotExist
from app.crud.queries.queries import queries
from app.crud.repositories.BaseRepository import BaseRepository


class UserRepository(BaseRepository):

    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_user(self, email, password) -> List:
        records = await queries.get_user_by_email(self.connection, email, password)
        if records is None:
            return []
        return [self.map_row(r) for r in records]

    @staticmethod
    def map_row(row: Record):
        return {
            'userId': row.get('user_id'),
            'name': row.get('name')
        }
