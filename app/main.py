from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from app.api import live
from app.api.router import api_router
from app.core.events import create_start_app_handler, create_stop_app_handler


def get_app():
    application = FastAPI()
    application.include_router(live.router)
    application.add_event_handler("startup", create_start_app_handler(application))
    application.add_event_handler("shutdown", create_stop_app_handler(application))
    application.include_router(api_router, prefix='/c2c')

    application.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return application


app = get_app()
if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, port=7000)
