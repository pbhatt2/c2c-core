from enum import Enum

from pydantic.main import BaseModel
import typing


class Request(BaseModel):
    imageData: str
    skuId: str


class Status(str, Enum):
    SUCCESS = "OK"
    ERROR = "ERROR"


class Response(BaseModel):
    status: Status = Status.SUCCESS
    image_url: str = None
    predicted_category: str = None
    predicted_class: str = None
    confidence: str = None