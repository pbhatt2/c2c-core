from datetime import datetime, timezone, timedelta
from enum import Enum

from pydantic.main import BaseModel
from pydantic.json import UUID
from typing import List


class Request(BaseModel):
    name: str=''


class Status(str, Enum):
    SUCCESS = "OK"
    ERROR = "ERROR"


class Response(BaseModel):
    status: Status = Status.SUCCESS


class SkuResponse(BaseModel):
    status: Status = Status.SUCCESS
    sku_id: str = None


class ProductCreateResponse(BaseModel):
    status: Status = Status.SUCCESS
    product_id: str

class ProductCreateRequest(BaseModel):
    product_id: str = None
    name: str 
    description: str
    brand_name: str
    categories: List[str]


class GetProductResponse(BaseModel):
    product_id: str = None
    name: str = None
    description: str = None
    brand_name: str = None
    categories: List[str] = None


class GetAllProductsResponse(BaseModel):
    products : List[GetProductResponse]


class BidCreateRequest(BaseModel):
    cust_id: str 
    sku_id: str 
    bid_price: int 
    currency: str
    isaccepted: bool


class BidResponse(BaseModel):
    bid_id: str = None
    cust_id: str = None 
    sku_id: str = None
    bid_price: int = None
    currency: str = None
    isaccepted: bool = False


class GetAllBidsResponse(BaseModel):
    bids: List[BidResponse]


class Bid(BaseModel):
    enabled: bool = True
    type: str = 'Offer'
    range: int = 50
    startDate: datetime = datetime.now(timezone.utc)
    endDate: datetime = datetime.now(timezone.utc) + timedelta(days=7)


class SkuCreateRequest(BaseModel):
    product_id: str = None
    sku_id: str = None
    seller_id: str = None
    seller_rating: int = 0
    name: str = None
    evaluated_condition: str = 'Good'
    categories: List[str] = None
    quantity: int
    condition: str
    description: str
    brand_name: str
    medialist: List[str]
    price: dict
    bid: Bid
    is_used: bool = True


class Filters(BaseModel):
    key: str = None
    values: List[str] = None


class SearchRequest(BaseModel):
    query: str = None
    category: List[str] = None
    filters: List[Filters] = None


class SearchResponse(BaseModel):
    status: Status = Status.SUCCESS
    data: dict = None
