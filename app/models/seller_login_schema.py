from enum import Enum

from pydantic.main import BaseModel


class SellerLoginRequest(BaseModel):
    email: str
    password: str


class Status(str, Enum):
    SUCCESS = "OK"
    ERROR = "ERROR"


class SellerLoginResponse(BaseModel):
    status: Status = Status.SUCCESS
    seller_id: str
    name: str
