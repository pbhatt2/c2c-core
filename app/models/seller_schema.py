from enum import Enum

from pydantic.main import BaseModel


class SellerRegisterRequest(BaseModel):
    name: str
    mobile: str
    email: str
    password: str
    address: str
    shipmentMethod: str


class Status(str, Enum):
    SUCCESS = "OK"
    ERROR = "ERROR"


class SellerRegisterResponse(BaseModel):
    status: Status = Status.SUCCESS
    seller_id: str