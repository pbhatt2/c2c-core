from enum import Enum

from pydantic.main import BaseModel


class UserLoginRequest(BaseModel):
    email: str
    password: str


class Status(str, Enum):
    SUCCESS = "OK"
    ERROR = "ERROR"


class UserLoginResponse(BaseModel):
    status: Status = Status.SUCCESS
    user_id: str
    name: str