# pylint: disable=no-name-in-module,unspecified-encoding
import json


class Singleton(type):
    """
    make a class Singleton
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def read_json(file_path):
    """
    read a json file
    :param file_path: json file path
    :return: python dictionary
    """
    with open(file_path, "r") as j_file:
        data = json.load(j_file)
    return data


def flatten(data, separator='.'):
    data = json.loads(data)
    klist = ['price', 'bid']
    for keys in klist:
        obj = data[keys]
        for key, values in obj.items():
            data[keys+separator+key] = values
        del data[keys]
    return json.dumps(data)
