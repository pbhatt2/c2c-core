import psycopg2
from psycopg2 import Error

try:
    # Connect to an existing database
    connection = psycopg2.connect(user="postgres",
                                  password="password",
                                  host="localhost",
                                  port="5432",
                                  database="postgres")

    # Create a cursor to perform database operations
    cursor = connection.cursor()
    # Print PostgreSQL details
    print("PostgreSQL server information")
    print(connection.get_dsn_parameters(), "\n")

    #Create Tabels
    #Product
    cursor.execute('''
    create table product 
    ( product_id TEXT , name TEXT , description TEXT , brand_name TEXT , categories TEXT[] );
    ''')

    #seller
    cursor.execute('''
    CREATE TABLE seller
    (seller_id TEXT, email TEXT, mobile TEXT, password TEXT, address TEXT, shipment_method TEXT, name TEXT);
    ''')

    #bid 
    cursor.execute('''
    create table bid 
    ( bid_id TEXT , sku_id TEXT , cust_id TEXT , bid_price INTEGER , currency TEXT , isaccepted BOOLEAN );
    ''')

    #Insert Values
    cursor.execute('''
    INSERT INTO product ( product_id  , name  , description , brand_name , categories ) 
    VALUES 
    ('628444be-76c3-411d-b7a3-f927dcd8d1ef','Apple Iphone 11', 'Apple Iphone 11 with 128GB' , 'Apple' , '{"Electronics","Mobile"}'),
    ('ed5c0c49-df92-4887-840e-14ccd1a0d96d','Macbook pro', 'Macbook pro 16 inch 16 gb ram' , 'Apple' , '{"Electronics","Laptop"}'),
    ('27783466-f860-4097-9e0b-8ee7ed54e2b4','Shoes', 'Running shoes' , 'Nike' , '{"Fashion","Footwear"}'),
    ('afcefb72-b311-4c92-920f-a08974ccb1f2','Smart Tv', 'Smart Tv with 42 inch' , 'Samsung' , '{"Electronics","TV"}'),
    ('07cdac05-3533-4c53-bca8-fe6874e29aad','Bagpack', 'Daily use bagpack for engineers' , 'one8' , '{"Fashion","Bags"}');
    ''')

    connection.commit()
    
except (Exception, Error) as error:
    print("Error while connecting to PostgreSQL", error)
finally:
    if (connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")