CREATE TABLE product ( product_id TEXT , name TEXT , description TEXT , brand_name TEXT , categories TEXT[] );
CREATE TABLE seller(seller_id TEXT, email TEXT, mobile TEXT, password TEXT, address TEXT, shipment_method TEXT, name TEXT);
CREATE TABLE bid ( bid_id TEXT , sku_id TEXT , cust_id TEXT , bid_price INTEGER , currency TEXT , isaccepted BOOLEAN );


CREATE TABLE sku ( sku_id TEXT , seller_id TEXT , product_id TEXT , bid_enabled BOOLEAN DEFAULT true , bid_start_date timestamptz DEFAULT NOW() , bid_end_date timestamptz, data jsonb );
CREATE TABLE images ( image_id TEXT , sku_id TEXT , url TEXT);